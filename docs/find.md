# Find

Write your own 'find' command (exec `man find` in your shell to find out what
this method does).

Don't worry, you wont implement the whole command... Just a few flags the we
have decided are cool enough for you to implement, and will not cause to much
trouble.

## Stage 1

Let's start with basic `find`.

Create new branch called `find-stage-1` and write all your code in `src/find.sh` file.

Implemet the basic `find [path] -name [expression]` and `find [path] -iname [expression]` variations.
The output should be similar to the actual `find` output.

So for this directory tree:
[![directory-tree](https://i.imgur.com/d7M16z2.png)]()

The output should be:
[![stage1-output](https://imgur.com/NVSW0BR.png)]()

After this stage open new PR (pull request, here in gitlab it called merge request) and update your trainer. 

## Stage 2

In this stage you will implement `-maxdepth` and `-mindepth` flags.

Create new branch **on `find-stage-1`** called `find-stage-2`.

For the previous directory tree the output should look like:
[![stage2-output](https://imgur.com/cIbpgEe.png)]()

After this stage open new PR and update your trainer.

## Stage 3

Create new branch called `find-stage-3` on `find-stage-2` and implement the `-L` flag.
Read about symlinks and implement it.

# Bonus:
Implement the `-P` flag. 
